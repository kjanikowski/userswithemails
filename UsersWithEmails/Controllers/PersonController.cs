﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using UsersWithEmails.Controllers.DTOs;
using UsersWithEmails.Data;
using UsersWithEmails.Models;

namespace UsersWithEmails.Controllers
{
    public class PersonController : Controller
    {
        private readonly IMapper mapper;
        private readonly IPersonRepository personRepository;
        public PersonController(IMapper mapper, IPersonRepository personRepository)
        {
            this.mapper = mapper;
            this.personRepository = personRepository;
        }

        public async Task<IActionResult> Index()
        {
            var persons = await personRepository.GetAllAsync();
            return View(mapper.Map<List<PersonDTO>>(persons));
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(PersonFromDTO personFromDTO)
        {
            var person = mapper.Map<Person>(personFromDTO);
            await personRepository.SaveAsync(person);
            return View();
        }

        [HttpGet("Person/Delete/{personID}")]
        public async Task<IActionResult> Delete(long personID)
        {
            var person = await personRepository.GetPersonByIdAsync(personID);
            await personRepository.DeleteAsync(person);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Edit(long id)
        {
            var person = await personRepository.GetPersonByIdAsync(id);

            return View(person);
        }

        [HttpPost, ActionName("Edit")]
        public async Task<IActionResult> EditPerson(long id, PersonFromDTO personFromDTO)
        {
            var person = await personRepository.GetPersonByIdAsync(id);
            person.Update(personFromDTO.FirstName, personFromDTO.LastName, personFromDTO.Remarks);
            await personRepository.UpdateAsync(person);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> GeneratePersons()
        {
            var persons = PersonGenerator.GeneratePersonsWithEmail();
            await personRepository.SaveRangeAsync(persons);

            return RedirectToAction("Index");
        }
    }
}
