﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using UsersWithEmails.Controllers.DTOs;
using UsersWithEmails.Data;
using UsersWithEmails.Models;

namespace UsersWithEmails.Controllers
{
    public class EmailController : Controller
    {
        private readonly IMapper mapper;
        private readonly IEmailRepository emailRepository;
        public EmailController(IMapper mapper, IEmailRepository emailRepository)
        {
            this.mapper = mapper;
            this.emailRepository = emailRepository;
        }

        [HttpGet("Email/GetEmailForUser/{personId}")]
        public async Task<ActionResult> GetEmailForUser(long personId)
        {
            var emails = await emailRepository.GetAllEmailsForPersonIdAsync(personId);
            ViewBag.PersonID = personId;
            return View(mapper.Map<List<EmailDTO>>(emails));
        }

        [HttpGet("Email/Create/{personId}")]
        public IActionResult Create(long personId)
        {
            ViewBag.PersonId = personId;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(EmailFormDTO emailFromDTO)
        {
            var email = mapper.Map<Email>(emailFromDTO);
            await emailRepository.SaveAsync(email);
            return RedirectToAction("GetEmailForUser", new { id = emailFromDTO.PersonId });
        }

        [HttpGet("Email/Delete/{emailID}")]
        public async Task<IActionResult> Delete(long emailID)
        {
            var email = await emailRepository.GetEmailByIdAsync(emailID);
            await emailRepository.DeleteAsync(email);
            return RedirectToAction("GetEmailForUser", new { id = email.PersonId });
        }

        public async Task<IActionResult> Edit(long id)
        {
            var email = await emailRepository.GetEmailByIdAsync(id);

            return View(email);
        }

        [HttpPost, ActionName("Edit")]
        public async Task<IActionResult> EditPerson(long id, EmailFormDTO emailFormDTO)
        {
            var email = await emailRepository.GetEmailByIdAsync(id);
            email.Update(emailFormDTO.Adress);
            await emailRepository.UpdateAsync(email);

            return RedirectToAction("GetEmailForUser", new { id = email.PersonId });
        }
    }
}
