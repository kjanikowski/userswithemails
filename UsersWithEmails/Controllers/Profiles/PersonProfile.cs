﻿using AutoMapper;
using UsersWithEmails.Controllers.DTOs;
using UsersWithEmails.Models;

namespace UsersWithEmails.Controllers.Profiles
{
    public class PersonProfile : Profile
    {
        public PersonProfile() 
        {
            CreateMap<PersonFromDTO, Person>();



            CreateMap<Person, PersonDTO>()
                .ForMember(result => result.FirstEmail, o => o.MapFrom(src => src.Emails.FirstOrDefault().Adress));
        }

    }
}
