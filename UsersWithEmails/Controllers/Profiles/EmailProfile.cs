﻿using AutoMapper;
using UsersWithEmails.Controllers.DTOs;
using UsersWithEmails.Models;

namespace UsersWithEmails.Controllers.Profiles
{
    public class EmailProfile : Profile
    {
        public EmailProfile()
        {
            CreateMap<Email,EmailDTO>();
            CreateMap<EmailFormDTO, Email>();
        }
    }
}
