﻿namespace UsersWithEmails.Controllers.DTOs
{
    public record EmailFormDTO
    {
        public string Adress { get; init; }
        public long PersonId { get; init; }
    }
}
