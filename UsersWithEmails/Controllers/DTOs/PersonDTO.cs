﻿using UsersWithEmails.Models;

namespace UsersWithEmails.Controllers.DTOs
{
    public record PersonDTO
    {
        public string Id { get; init; }
        public string FirstName { get; init; }
        public string LastName { get; init; }
        public string Remarks { get; init; }
        public string FirstEmail { get; init; }
    }
}
