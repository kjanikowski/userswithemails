﻿namespace UsersWithEmails.Controllers.DTOs
{
    public record EmailDTO
    {
        public long Id { get; init; }
        public string Adress { get; init; }
    }
}
