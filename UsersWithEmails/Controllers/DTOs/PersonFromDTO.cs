﻿using Bogus.DataSets;
using System.ComponentModel.DataAnnotations;

namespace UsersWithEmails.Controllers.DTOs
{
    public record PersonFromDTO
    {
        [Required, StringLength(50, ErrorMessage = "Name length can't be more than 50.")]
        public string FirstName { get; init; }
        [Required, StringLength(50, ErrorMessage = "Name length can't be more than 50.")]
        public string LastName { get; init; }
        public string? Remarks { get; init; }
    }
}
