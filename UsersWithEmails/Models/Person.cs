﻿namespace UsersWithEmails.Models
{
    public class Person
    {
        public long Id { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string? Remarks { get; private set; }

        public virtual List<Email> Emails { get; private set; }

        public void Update(string firstName, string lastName, string remarks)
        {
            FirstName = firstName;
            LastName = lastName;
            Remarks = remarks;
        }
    }
}
