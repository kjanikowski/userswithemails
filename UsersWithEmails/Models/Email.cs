﻿namespace UsersWithEmails.Models
{
    public class Email
    {
        public long Id { get; private set; }
        public long PersonId { get; private set; }
        public virtual Person Person { get; private set; }
        public string Adress { get; private set; }

        public void Update(string adress)
        {
            Adress = adress;
        }
    }
}
