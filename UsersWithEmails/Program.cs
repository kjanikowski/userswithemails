using Microsoft.EntityFrameworkCore;
using UsersWithEmails.Data;

namespace UsersWithEmails
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            var configuration = builder.Configuration;

            builder.Services.AddDbContext<UsersWithEmailsContext>(o => 
                o.UseSqlServer(configuration.GetConnectionString("UsersWithEmailsDb")));

            builder.Services.AddScoped<IPersonRepository, PersonRepository>();
            builder.Services.AddScoped<IEmailRepository, EmailRepository>();

            builder.Services.AddAutoMapper(typeof(Program), typeof(UsersWithEmailsContext));

            builder.Services.AddControllersWithViews();

            var app = builder.Build();

            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Person}/{action=Index}/{id?}");

            app.Run();
        }
    }
}