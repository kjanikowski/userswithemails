﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UsersWithEmails.Migrations
{
    /// <inheritdoc />
    public partial class ChangeNamesToEnglish : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Opis",
                table: "Person");

            migrationBuilder.RenameColumn(
                name: "Nazwisko",
                table: "Person",
                newName: "LastName");

            migrationBuilder.RenameColumn(
                name: "Imie",
                table: "Person",
                newName: "FirstName");

            migrationBuilder.AddColumn<string>(
                name: "Remarks",
                table: "Person",
                type: "nvarchar(max)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Remarks",
                table: "Person");

            migrationBuilder.RenameColumn(
                name: "LastName",
                table: "Person",
                newName: "Nazwisko");

            migrationBuilder.RenameColumn(
                name: "FirstName",
                table: "Person",
                newName: "Imie");

            migrationBuilder.AddColumn<string>(
                name: "Opis",
                table: "Person",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
