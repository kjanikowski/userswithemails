﻿using Bogus;
using UsersWithEmails.Models;
using Person = UsersWithEmails.Models.Person;

namespace UsersWithEmails.Data
{
    public static class PersonGenerator
    {
        public static List<Person> GeneratePersonsWithEmail()
        {
            return new Faker<Person>()
                .RuleFor(p => p.FirstName, f => f.Person.FirstName)
                .RuleFor(p => p.LastName, f => f.Person.LastName)
                .RuleFor(p => p.Remarks, f => f.Random.String2(50))
                .RuleFor(p => p.Emails, () => GenerateEmail())
                .Generate(5);
        }

        private static List<Email> GenerateEmail()
        {
            return new Faker<Email>()
                .RuleFor(e => e.Adress, f => f.Person.Email)
                .Generate(3);
        }
    }
}
