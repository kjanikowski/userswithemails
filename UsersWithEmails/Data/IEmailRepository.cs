﻿using UsersWithEmails.Models;

namespace UsersWithEmails.Data
{
    public interface IEmailRepository
    {
        Task<List<Email>> GetAllEmailsForPersonIdAsync(long personId);
        Task SaveAsync(Email email);
        Task DeleteAsync(Email email);
        Task<Email> GetEmailByIdAsync(long id);
        Task UpdateAsync(Email email);
    }
}
