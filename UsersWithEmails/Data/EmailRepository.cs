﻿using Microsoft.EntityFrameworkCore;
using UsersWithEmails.Models;

namespace UsersWithEmails.Data
{
    public class EmailRepository : IEmailRepository
    {
        private readonly UsersWithEmailsContext context;
        public EmailRepository(UsersWithEmailsContext context)
        {
            this.context = context;
        }

        public Task<List<Email>> GetAllEmailsForPersonIdAsync(long personId)
        {
            return context.Emails
                .Where(e => e.PersonId == personId)
                .ToListAsync();
        }

        public Task SaveAsync(Email email)
        {
            context.Add(email);
            return context.SaveChangesAsync();
        }

        public Task DeleteAsync(Email email)
        {
            context.Emails.Remove(email);
            return context.SaveChangesAsync();
        }

        public Task<Email> GetEmailByIdAsync(long id)
        {
            return context.Emails.FirstOrDefaultAsync(p => p.Id == id);
        }

        public Task UpdateAsync(Email email)
        {
            context.Emails.Update(email);
            return context.SaveChangesAsync();
        }
    }
}
