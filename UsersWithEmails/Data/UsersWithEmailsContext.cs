﻿using Microsoft.EntityFrameworkCore;
using UsersWithEmails.Models;

namespace UsersWithEmails.Data
{
    public class UsersWithEmailsContext : DbContext
    {
        public UsersWithEmailsContext(DbContextOptions<UsersWithEmailsContext> options) : base(options) 
        { 
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Email> Emails { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var personBuilder = modelBuilder.Entity<Person>();
            personBuilder.ToTable("Person").HasKey(k => k.Id);
            personBuilder.Property(p => p.FirstName).HasMaxLength(50);
            personBuilder.Property(p => p.LastName).HasMaxLength(50);
            personBuilder.Property(p => p.Remarks);

            var emailBuilder = modelBuilder.Entity<Email>();
            emailBuilder.ToTable("Email").HasKey(k => k.Id);
            emailBuilder.Property(p => p.Adress).HasMaxLength(50);
            emailBuilder.HasOne(p => p.Person)
            .WithMany(q => q.Emails)
            .HasForeignKey(p => p.PersonId);
        }
    }
}
