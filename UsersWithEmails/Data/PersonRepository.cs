﻿using Microsoft.EntityFrameworkCore;
using UsersWithEmails.Models;

namespace UsersWithEmails.Data
{
    public class PersonRepository : IPersonRepository
    {
        private readonly UsersWithEmailsContext context;
        public PersonRepository(UsersWithEmailsContext context)
        {
            this.context = context;
        }

        public Task SaveAsync(Person person)
        {
            context.Add(person);
            return context.SaveChangesAsync();
        }

        public Task<List<Person>> GetAllAsync()
        {
            return context.Persons
                .Include(p => p.Emails)
                .ToListAsync();
        }

        public Task DeleteAsync(Person person)
        {
            context.Persons.Remove(person);
            return context.SaveChangesAsync();
        }

        public Task<Person> GetPersonByIdAsync(long id)
        {
            return context.Persons.FirstOrDefaultAsync(p => p.Id == id);
        }

        public Task UpdateAsync(Person person) 
        {
            context.Persons.Update(person);
            return context.SaveChangesAsync();
        }

        public Task SaveRangeAsync(List<Person> persons)
        {
            context.AddRangeAsync(persons);
            return context.SaveChangesAsync();
        }
    }
}
