﻿using UsersWithEmails.Models;

namespace UsersWithEmails.Data
{
    public interface IPersonRepository
    {
        Task SaveAsync(Person person);
        Task<List<Person>> GetAllAsync();
        Task DeleteAsync(Person person);
        Task<Person> GetPersonByIdAsync(long id);
        Task UpdateAsync(Person person);
        Task SaveRangeAsync(List<Person> persons);
    }
}
